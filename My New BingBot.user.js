// ==UserScript==
// @name         My New BingBot
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  performs a search action in bing clicks on specific site if it is in output and surfs it
// @author       Archibald Gustavsson
// @match        https://www.bing.com/*
// @match        https://www4.bing.com/*
// @match        http://samyitupoi.narod.ru/*
// @match        http://www.samyitupoi.narod.ru/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

let links = document.links;
let searchIcon = document.getElementById("search_icon");
let keywords = ["Самый тупой сайт",
                "очень тупой, тупее нет",
                "Yarrrrrrrrr",
                "Музыка к сериалу 'Шерлок Холмс'"];
let keyword = keywords[getRandom(0, keywords.length)];
// entering search queue
if (searchIcon != undefined) {
  let i = 0;
  let timerId = setInterval(function(){
      if (i >= keyword.length) {
          clearInterval(timerId);
          searchIcon.click();
      } else {
          let searchInput = document.getElementsByName("q")[0];
          searchInput.value += keyword[i];
          i=i+1;
      }
  },1000);
    // target site by ancient webmaster (i wonder where he is now)
} else if (location.hostname == 'samyitupoi.narod.ru') {
    setInterval(()=>{
        let index = getRandom(0, links.length);
        if (getRandom(0, 101) >= 70) {
            location.href = "https://bing.com/";
        }
        if (links[index].href.indexOf('samyitupoi.narod.ru') != -1) links[index].click();
    }, getRandom(2500, 5000));
} else {
    // looking for target site after sending request
    for (let i = 0; i < links.length; i++) {
        if (links[i].href.indexOf("samyitupoi.narod.ru") != -1) {
            if (getRandom(0, 101) >= 90) {
                location.href = "https://www.bing.com/";
            }
            let link = links[i];
            console.log(link+' is found');
            setTimeout(()=>{link.click()}, getRandom(2000,3500));
            break;
        }
    }
}

// next page select if it wasn't found at current
let checkPage = document.querySelector("#b_results > li.b_pag > nav > ul > li:nth-child(4) > a");
if (!checkPage.hasAttribute("aria-label")) {
    setTimeout(()=>{location.href = "https://bing.com/";}, getRandom(3000,4500));
} else {
    let nextPage = document.querySelector('[title="Следующая страница"]');
    setTimeout(()=>{nextPage.click();}, getRandom(3000,4500));
}

// generate random number
function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}